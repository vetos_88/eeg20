document.getElementById("search_field").addEventListener("input", function () {
    var search_field = document.getElementById("search_field");
    var search_text = search_field.value;
    var search_result_field = document.getElementById("search_advice_wrapper");
    if (search_text.length >= 2) {
        var request = new XMLHttpRequest();
        var requestURL = "search/" + search_text; //убрать хардкод
        request.open("GET", requestURL, true);
        request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        request.onreadystatechange = function() {
          if (request.readyState == 4) {
            var response = request.responseText;
            search_result_field.innerHTML = response;
          }
        };
        request.send(null);

    }
    else{
        search_result_field.innerHTML = " ";
    }
})
