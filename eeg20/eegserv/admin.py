from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Group

from .utils import export_as_csv_action
from .adminutils import RegexModelAdmin
from .forms import *
from .models import ExtUser, Track


class TrackInline(admin.StackedInline):
    model = Track
    extra = 0


@admin.register(Track)
class TrackAdmin(admin.ModelAdmin):

    # form = TrackCreateForm

    # def save_model(self, request, obj, form, change):
    #     super.save_model(self, request, obj, form, change)


    # list_editable = ('comment', )

    raw_id_fields = ('user',)  # сделать ввод пользователя через логин

    readonly_fields = (
        'create_date',
        'upd_date',
    )

    # add_fieldsets = (
    #     (None, {
    #         'fields': (
    #             'user',
    #             'data',
    #             'comment',
    #         )}
    #      ),
    # )

    fieldsets = (
        (None, {
            'fields': (
                'user',
                'data',
                'comment',
            )}
         ),
        ('Создан, редактирован', {
            'fields': (
                ('create_date', 'upd_date'),
            )}
         ),
    )


    list_display = [
        'upd_date',
        'create_date',
        'comment',
        'user',
    ]

    # fields = [
    #     'user',
    #     'comment',
    #     'data',
    # ]

    list_display_links = [
        'upd_date',
    ]

    list_filter = [
        'upd_date',
        'user__login',
    ]

    search_fields = [
        '/user__surname/',
        '/user__name/',
        '/user__second_name/',
        '/user__login/'
    ]

    # date_hierarchy = 'updated_date'

    actions = [
        export_as_csv_action(
            'Экспорт в CSV',
            fields=[
                'id',
                'get_user',
                'user',
                'upd_date',
                'data',
            ]
        )
    ]

    # list_per_page = 10


@admin.register(ExtUser)
class ExtUserAdmin(UserAdmin):
    form = UserChangeForm
    add_form = UserCreationForm

    inlines = [
        TrackInline,
    ]

    list_display = [
        'id',
        'login',
        'lastname',
        'firstname',
        'middlename',
        'date_joined',
        'last_login',
        'email',
        'date_of_birth',
    ]

    list_filter = ('is_admin',)

    fieldsets = (
        (None, {'fields': ('login', 'password', 'email')}),
        ('Personal info', {
            'fields': (
                'lastname',
                'firstname',
                'middlename',
                'date_of_birth',
            )}),
        ('Permissions', {'fields': ('is_admin', 'is_active', 'is_superuser', )}),

    )

    add_fieldsets = (
        (None, {
            'fields': (
                'login',
                'password1',
                'password2',
            )}
         ),
    )

    list_display_links = [
        'id',
        'login',
    ]

    search_fields = ('login', 'email',)
    ordering = ('id', 'login', 'lastname', 'firstname', 'middlename',)
    filter_horizontal = ()


admin.site.unregister(Group)
