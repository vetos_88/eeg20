from django.apps import AppConfig


class EegservConfig(AppConfig):
    name = 'eegserv'
