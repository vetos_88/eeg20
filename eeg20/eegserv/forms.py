from django import forms
from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist
import numpy as np
import json

from .models import Track, ExtUser


class UserCreationForm(forms.ModelForm):
    password1 = forms.CharField(
        label='Пароль',
        widget=forms.PasswordInput

    )
    password2 = forms.CharField(
        label='Подтверждение',
        widget=forms.PasswordInput
    )

    def clean(self):
        cleaned_data = self.cleaned_data
        login = cleaned_data.get('login')
        if login:
            cleaned_data['login'] = login.lower()
        return cleaned_data

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError('Пароль и подтверждение не совпадают')
        return password2

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        if commit:
            user.save()
        return user

    class Meta:
        model = get_user_model()
        fields = ('login',)


class UserChangeForm(forms.ModelForm):

    #

    # def save(self, commit=True):
    #     user = super(UserChangeForm, self).save(commit=False)
    #     if password.
    #     password = self.password
    #     if password:
    #         user.set_password(password)
    #     if commit:
    #         user.save()
    #     return user

    # last_login = forms.DateField(
    #     disabled=False,
    #     required=False
    # )
    # date_joined = forms.DateField(
    #     disabled=False,
    #     required=False
    # )

    class Meta:
        model = get_user_model()
        fields = ('login', )


class LoginForm(forms.Form):

    login = forms.CharField(
        label='Пользователь',
    )
    password = forms.CharField(
        label='Пароль',
        widget=forms.PasswordInput,
    )


class TrackCreateForm(forms.ModelForm):

    user = forms.ModelChoiceField(
        label='Пациент',
        queryset=ExtUser.objects.all(),
        initial='anonymous',
        to_field_name='login',
        # widget=widgets. разобраться с виджетом выбора пользователя
    )
    data = forms.FileField(
        label='Трек',
    )
    comment = forms.CharField(
        label='Коммент',
        max_length=255,
        required=False,
    )

    class Meta:
        model = Track
        fields = ['user', 'data', 'comment']


class DataSendingForm(forms.ModelForm):

    user = forms.CharField(
        label='Пациент',

    )

    data = forms.FileField(
        label='Трек',
    )

    comment = forms.CharField(
        label='Коммент',
        max_length=255,
        required=False,
    )

    PACK_TYPE_CHOICES = (
        ('single', 'Одним пакетом',),
        ('start', 'Стартовый',),
        ('middle', 'Промежуточный',),
        ('end', 'Конечный',),

    )

    chunk = forms.ChoiceField(
        label='Тип пакета',
        widget=forms.RadioSelect,
        choices=PACK_TYPE_CHOICES,
    )

    num_chunk = forms.IntegerField(
        label='Номер пакета',
        min_value=0,
        required=False,
    )

    # def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None,
    #              initial=None, error_class=ErrorList, label_suffix=None,
    #              empty_permitted=False, instance=None, use_required_attribute=None):
    #     super().__init__(data=None, files=None, auto_id='id_%s', prefix=None, initial=None, error_class=ErrorList,
    #                      label_suffix=None, empty_permitted=False, instance=None, use_required_attribute=None)
    #
    #     self.MAX_FILE_SIZE = 200 * 1024 * 1024

    def clean_user(self):

        login = self.cleaned_data.get('user')

        try:
            user = ExtUser.objects.get(login=login)
        except ObjectDoesNotExist:
            raise forms.ValidationError("User not found")
        else:
            print(user)
            return user

    def clean_data(self):

        MAX_FILE_SIZE = 200 * 1024 * 1024

        file = self.cleaned_data['data']
        file_size = file.size

        if file_size > MAX_FILE_SIZE:
            raise forms.ValidationError("Very large file")

        raw_data = file.read().decode('utf-8')

        try:
            json_data = json.loads(raw_data)
        except (TypeError, ValueError):
            raise forms.ValidationError("Incorrect JSON format")
        else:
            json_2d_array_of_int32 = np.array(json_data)

            data_dtype = json_2d_array_of_int32.dtype
            data_shape = json_2d_array_of_int32.shape

            print(data_dtype)
            print(data_shape)

            datacorrect = (
                (
                 data_dtype == np.dtype('int32')
                 or
                 data_dtype == np.dtype('float64')
                )
                and
                len(data_shape) == 2
            )

            if not datacorrect:
                raise forms.ValidationError("Incorrect data format!"
                                            "Try file with JSON array_of_arrays-like structure")
            return file

    def clean(self):

        cleaned_data = super(DataSendingForm, self).clean()

        chunk = cleaned_data.get('chunk')
        num_chunk = cleaned_data.get('num_chunk')

        if chunk == 'middle' and not num_chunk:
            print('In middle err')
            raise forms.ValidationError("No data part with "
                                        "middle pack type.")

        return cleaned_data

    class Meta:
        model = Track
        # exclude = []
        fields = ['user', 'data', 'comment', 'chunk']

