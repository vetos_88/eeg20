import re

from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render, redirect, render_to_response
from django.contrib import auth

from eegserv.forms import LoginForm, UserCreationForm, UserChangeForm, TrackCreateForm, DataSendingForm
from eegserv.models import ExtUser


def main(request):
    loginform = LoginForm()
    createform = UserCreationForm()
    # createform = UserChangeForm()
    dataform = DataSendingForm()
    return render(request,
                  'index.html',
                  {'loginform': loginform,
                   'createform': createform,
                   'dataform': dataform,
                   })


def login(request):
    if request.method == 'POST':
        userlogin = request.POST['login']
        userlogin = userlogin.lower()
        password = request.POST['password']
        user = auth.authenticate(username=userlogin, password=password)

        if user:
            if user.is_active:
                auth.login(request, user)
                response = 'login {} ok'.format(userlogin)
                return redirect('main')
            else:
                response = 'disabled account'
                return HttpResponse(response)
        else:
            response = 'invalid login or password'
            return HttpResponse(response)
    return HttpResponse('bad request')


def logout(request):
    auth.logout(request)
    return redirect('main')


def createuser(request):
    if request.method == 'POST':
        post = request.POST
        createform = UserCreationForm(post)
        if createform.is_valid():
            createform.save()
            cleaned_data = createform.cleaned_data
            created_login = cleaned_data['login']
            response = 'user {} was created'.format(created_login)
            return HttpResponse(response)
        # else:
        #     errors = form.errors
        #     return render_to_response('errors.html',
        #                               {'errors': errors},
        #                               status=400)
    else:
        createform = UserCreationForm()
    return render(request,
                  'createform.html',
                  {'createform': createform})


def search(request, name):
    patt = re.compile(r"\s+")
    name = patt.sub(" ", name)
    full_name = name.split()
    help_login = request.META.get("HTTP_HELP_LOGIN")
    users = ExtUser.objects.filter(Q(lastname__iregex=full_name[0]) |
                                   Q(firstname__iregex=full_name[0]) |
                                   Q(firstname__iregex=full_name[0]) |
                                   Q(login__iregex=full_name[0]))
    if len(full_name) == 2:
        users = users.filter(Q(lastname__iregex=full_name[1]) |
                             Q(firstname__iregex=full_name[1]) |
                             Q(middlename__iregex=full_name[1]))
    if len(full_name) == 3:
        users = users.filter(Q(lastname__iregex=full_name[2]) |
                             Q(firstname__iregex=full_name[2]) |
                             Q(middlename__iregex=full_name[2]))

    count_find_users = len(users)
    if count_find_users:
        if help_login:
            return render_to_response('searchresult.html',
                                      {'logins': users},
                                      status=302)
        else:
            return render_to_response('searchresult.html',
                                      {'users': users},
                                      status=302)
    else:
        response = "No users"
        return HttpResponse(response)


def eegdata(request):
    if request.method == 'POST':
        post = request.POST
        trackfile = request.FILES
        # print(trackfile)
        dataform = DataSendingForm(post, trackfile)
        print(dataform.is_valid())
        if dataform.is_valid():
            # file = dataform.cleaned_data['data']
            # print(file)
            dataform.save()
            return render(request,
                          'dataSendingform.html',
                          {'dataform': dataform})
    else:
        dataform = DataSendingForm()
    return render(request,
                  'dataSendingform.html',
                  {'dataform': dataform})


def display_meta(request):
    # ExtUser.objects.get(login="etrerertq")
    values = request.META.items()
    values = dict(values)
    keys = list(values.keys())
    keys.sort()
    html = []
    for k in keys:
        html.append('<tr><td>%s</td><td>%s</td></tr>' % (k, values[k]))

    return HttpResponse('<table>%s</table>' % '\n'.join(html))
