

from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.models import BaseUserManager
from django.db import models
from datetime import datetime, date, time, timezone


class UserManager(BaseUserManager):

    def create_user(self, login, password=None):
        if not login:
            raise ValueError('Логин непременно должен быть указан')

        user = self.model(
            login=login.lower(),
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, login, password):
        user = self.create_user(login, password)
        user.is_admin = True
        user.save(using=self._db)
        return user


class ExtUser(AbstractBaseUser, PermissionsMixin):

    login = models.CharField(
        'Логин',
        max_length=255,
        unique=True,
        db_index=True,

    )
    email = models.EmailField(
        'Электронная почта',
        max_length=255,
        null=True,
        blank=True

    )
    firstname = models.CharField(
        'Имя',
        max_length=40,
        null=True,
        blank=True
    )
    lastname = models.CharField(
        'Фамалия',
        max_length=40,
        null=True,
        blank=True
    )
    middlename = models.CharField(
        'Отчество',
        max_length=40,
        null=True,
        blank=True
    )
    date_of_birth = models.DateField(
        'Дата рождения',
        null=True,
        blank=True,
        help_text='дд.мм.гггг'
    )
    date_joined = models.DateField(
        'Создан',
        auto_now_add=True,
        editable=False,
    )
    last_login = models.DateTimeField(
        'Последняя регистрация',
        auto_now=True,
        null=True,
        editable=False,
    )
    is_active = models.BooleanField(
        'Активен',
        default=True
    )
    is_admin = models.BooleanField(
        'Доступ в админку',
        default=False
    )
    is_superuser = models.BooleanField(
        'Суперпользователь',
        default=False
    )

    def get_full_name(self):
        full_name = "{} {} {}".format(self.lastname, self.firstname, self.middlename)
        return full_name

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True
        # return True if self.is_admin else False

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True
        # return True if self.is_admin else False

    # Требуется для админки
    @property
    def is_staff(self):
        return self.is_admin

    def get_short_name(self):
        return self.login

    def __str__(self):
        return self.login

    def __unicode__(self):
        return self.login

    USERNAME_FIELD = 'login'
    REQUIRED_FIELDS = []

    objects = UserManager()

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'


class Track(models.Model):

    def get_file_path(self, filename):
        path = '{}\\{}'.format(self.user.login, datetime.now())
        return path

    user = models.ForeignKey(
        ExtUser,
        verbose_name='Пользователь',
        help_text='Логин пользователя трека',
        # default=ExtUser.objects.get(login='anonymous'),
    )
    comment = models.CharField(
        'Коментарий',
        max_length=255,
        null=True, blank=True,
        help_text='Опционально',
    )
    data = models.FileField(
        'Дынные',
        upload_to=get_file_path,
        null=False,
        blank=False,
        help_text='Файл в формате JSON'
    )
    create_date = models.DateField(
        'Создан',
        auto_now_add=True
    )
    upd_date = models.DateTimeField(
        'Обновлен',
        auto_now=True
    )

    def __str__(self):
        object_name = "{}; {}".format(self.create_date, self.user.login)
        return object_name

    def get_user(self):
        user_login = "{}".format(self.user.login)
        return user_login

    class Meta:
        ordering = ["-upd_date", "user"]
        verbose_name = "Трек"
        verbose_name_plural = "Треки"
