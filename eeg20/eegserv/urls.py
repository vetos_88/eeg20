from django.conf.urls import url, include
from .views import *
# from django.contrib.auth.views import login, logout


urlpatterns = [

    url(r'^headers', display_meta, name="display_meta"),
    url(r'^$', main, name="main"),
    url(r'^login/$', login, name="login"),
    url(r'^logout/$', logout, name="logout"),
    url(r'^create', createuser, name="create"),
    url(r'^search/(?P<name>.{2,20})', search, name="search"),
    url(r'^data', eegdata, name="data"),
    # # url(r'^search/(?P<name>.{2,20})', SearchUser.as_view(), name="search"),
    # url(r'^tracks', tracks, name="tracks"),
    # url(r'^track/(?P<trackid>\d+)', get_track, name="get_track"),

]
